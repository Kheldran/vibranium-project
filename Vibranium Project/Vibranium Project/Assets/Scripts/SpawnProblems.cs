﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProblems : MonoBehaviour
{
    public GameObject[] numberFloor;
    public int hazardCount;
    public GameObject[] allHazards;
    public List<GameObject> presentHazards = new List<GameObject>();
    public List<GameObject> unavailableSpawnPoints = new List<GameObject>();
    
    public int previousWindow;
    public GameObject previousPartBuilding;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < hazardCount; i++)
        {
            GameObject thisHazard;
            int randomHazard = Mathf.RoundToInt(Random.Range(0, allHazards.Length));
            int randomFloor = Mathf.RoundToInt(Random.Range(0, numberFloor.Length));
            while (unavailableSpawnPoints.Contains(numberFloor[randomFloor]))
            {
                randomFloor = Mathf.RoundToInt(Random.Range(0, numberFloor.Length));
            }
            int randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
            if (presentHazards.Count > 0)
            {
                if (presentHazards[i - 1].name.Substring(0, presentHazards[i - 1].name.Length - 1) == "1")
                {
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "4")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
                else if (presentHazards[i - 1].name.Substring(0, presentHazards[i - 1].name.Length - 1) == "4")
                {
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "1")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
            }
            thisHazard = Instantiate(allHazards[randomHazard], new Vector2(numberFloor[randomFloor].transform.GetChild(randomSpawn).transform.position.x, numberFloor[randomFloor].transform.GetChild(randomSpawn).transform.position.y), Quaternion.identity);
            thisHazard.transform.parent = numberFloor[randomFloor].transform.GetChild(randomSpawn).transform;
            presentHazards.Add(thisHazard);
            unavailableSpawnPoints.Add(numberFloor[randomFloor]);
            if (i == hazardCount - 1)
            {
                previousWindow = int.Parse(numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void SpawnHazards()
    {
        for (int i = 0; i < hazardCount; i++)
        {
            GameObject thisHazard;
            int randomHazard = Mathf.RoundToInt(Random.Range(0, allHazards.Length));
            int randomFloor = Mathf.RoundToInt(Random.Range(0, numberFloor.Length));
            while (unavailableSpawnPoints.Contains(numberFloor[randomFloor]))
            {
                randomFloor = Mathf.RoundToInt(Random.Range(0, numberFloor.Length));
            }
            int randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
            if (i == 0)
            {
                if (previousPartBuilding.GetComponent<SpawnProblems>().previousWindow == 1)
                {
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "4")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
                else if (previousPartBuilding.GetComponent<SpawnProblems>().previousWindow == 4)
                {
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "1")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
            }
            if (presentHazards.Count > 0)
            {
                if (presentHazards[i -1].name.Substring(0, presentHazards[i - 1].name.Length - 1) == "1"){
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "4")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
                else if (presentHazards[i - 1].name.Substring(0, presentHazards[i - 1].name.Length - 1) == "4")
                {
                    while (numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(0, numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1) == "1")
                    {
                        randomSpawn = Mathf.RoundToInt(Random.Range(0, numberFloor[randomFloor].transform.childCount));
                    }
                }
            }
            thisHazard = Instantiate(allHazards[randomHazard], new Vector2(numberFloor[randomFloor].transform.GetChild(randomSpawn).transform.position.x, numberFloor[randomFloor].transform.GetChild(randomSpawn).transform.position.y), Quaternion.identity);
            thisHazard.transform.parent = numberFloor[randomFloor].transform.GetChild(randomSpawn).transform;
            presentHazards.Add(thisHazard);
            unavailableSpawnPoints.Add(numberFloor[randomFloor]);
            if (i == hazardCount - 1)
            {
                previousWindow = int.Parse(numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Substring(numberFloor[randomFloor].transform.GetChild(randomSpawn).name.Length - 1));
            }
        }
    }
 

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlaceToDestroy")
        {
            for (int i = 0; i < presentHazards.Count; i++)
            {
                GameObject hazardToDestroy;
                hazardToDestroy = presentHazards[i];
                Destroy(hazardToDestroy);
            }
            presentHazards.Clear();
            unavailableSpawnPoints.Clear();



            SpawnHazards();
            if (gameObject.tag == "FirstBuilding")
            {
                print("boop");
                Destroy(gameObject);
            }
        }
    }
}
