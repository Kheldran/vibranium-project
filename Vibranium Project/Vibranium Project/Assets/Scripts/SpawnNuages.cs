﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNuages : MonoBehaviour
{
    public GameObject[] cloud;
    public Vector3 spawnValues;
    public int cloudCount;
    public float spawnWait;
    public float startWait;
    public float WaveWait;

    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < cloudCount; i++)
            {
                Vector3 spawnPosition = new Vector3(spawnValues.x, spawnValues.y, 0);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(cloud[Random.Range(0, cloud.Length)], spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(WaveWait);
        }
    }
}
