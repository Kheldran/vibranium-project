﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudBehaviour : MonoBehaviour
{
    public Vector2 velocity;

    private GameObject _backgroundScroll;
    private Rigidbody2D Body;

    void Awake()
    {
        Body = GetComponent<Rigidbody2D>();
        _backgroundScroll = GameObject.Find("BG1 (7)");
    }

    void FixedUpdate()
    {
        //Body.MovePosition(Body.position + velocity * Time.fixedDeltaTime * _backgroundScroll.GetComponent<BackgroundScroll>().acceleration);
        Body.MovePosition(new Vector2(Body.position.x + velocity.x * Time.fixedDeltaTime, (Body.position.y + velocity.y * Time.fixedDeltaTime * -_backgroundScroll.GetComponent<BackgroundScroll>().acceleration)));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "DestroyClouds")
        {
            Debug.Log(collision.gameObject.name);
            Destroy(gameObject);
        }
    }
}
