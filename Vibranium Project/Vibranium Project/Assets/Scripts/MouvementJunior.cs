﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvementJunior : MonoBehaviour
{

    private float _leftJoystickHorizontalValue;
    private float _leftJoystickVerticalValue;

    private float _rightJoystickHorizontalValue;
    private float _rightJoystickVerticalValue;

    private float _rightAndLeftAddition;
    private float _upAndDownAddition;
    public float _MinHValue;
    public float _MinVValue;
    public float _coefSpeed;

    public Camera _camera;

    private bool _hasHitLeft;
    private bool _hasHitRight;


    public GameObject _nacelle;
    public GameObject _player1;
    public GameObject _player2;

    private Nacelle _nacelleScript;

    public GameObject _backgroundScrollObject;
    private BackgroundScroll _backgroundScrollScript;

    public float _verticalSpeed;
    public float _speed;

    public GameObject _leftCleaner;
    public GameObject _rightCleaner;
    private Animator _rightCleanerAnim;
    private Animator _leftCleanerAnim;

    private GameButtons _gameButtonsScript;
    private string _PS4;

    void Start()
    {
        _nacelleScript = _nacelle.GetComponent<Nacelle>();
        _backgroundScrollScript = _backgroundScrollObject.GetComponent<BackgroundScroll>();
        _rightCleanerAnim = _rightCleaner.GetComponent<Animator>();
        _leftCleanerAnim = _leftCleaner.GetComponent<Animator>();
        _gameButtonsScript = GameObject.FindWithTag("GameController").GetComponent<GameButtons>();
        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            if (names[x].Length == 19)
            {
                _PS4 = "PS4";
            }
            if (names[x].Length == 33)
            {
                _PS4 = "";
            }
        }
        _speed = _speed / 100;
        _verticalSpeed = _verticalSpeed / 100;
    }

    // Update is called once per frame
    void Update()
    {

        if (!_gameButtonsScript._isPauseOn)
        {
            Vector3 _screenPosition = _camera.WorldToScreenPoint(this.transform.position);

            _rightJoystickHorizontalValue = Input.GetAxis("HorizontalRightJoystick" + _PS4);
            _rightJoystickVerticalValue = Input.GetAxis("VerticalRightJoystick" + _PS4);

            _leftJoystickHorizontalValue = Input.GetAxis("Horizontal");
            _leftJoystickVerticalValue = Input.GetAxis("VerticalLeft");

            _rightAndLeftAddition = _rightJoystickHorizontalValue + _leftJoystickHorizontalValue;
            _upAndDownAddition = _rightJoystickVerticalValue + _leftJoystickVerticalValue;

            if (!(_rightJoystickHorizontalValue > -_MinHValue && _rightJoystickHorizontalValue < _MinHValue) &&
                !(_leftJoystickHorizontalValue > -_MinHValue && _leftJoystickHorizontalValue < _MinHValue))
            {
                _rightAndLeftAddition *= _coefSpeed;
            }
            if (!(_rightJoystickVerticalValue > -_MinVValue && _rightJoystickVerticalValue < _MinVValue) &&
                !(_leftJoystickVerticalValue > -_MinVValue && _leftJoystickVerticalValue < _MinVValue))
            {
                _upAndDownAddition *= _coefSpeed;
            }

            if (_rightJoystickHorizontalValue != 0 || _rightJoystickVerticalValue != 0)
            {
                _rightCleanerAnim.SetBool("_isMoving", true);
            }

            else if (_rightJoystickHorizontalValue == 0 && _rightJoystickVerticalValue == 0)
            {
                _rightCleanerAnim.SetBool("_isMoving", false);
            }

            if (_leftJoystickHorizontalValue != 0 || _leftJoystickVerticalValue != 0)
            {
                _leftCleanerAnim.SetBool("_isMoving", true);
            }
            else if (_leftJoystickHorizontalValue == 0 && _leftJoystickVerticalValue == 0)
            {
                _leftCleanerAnim.SetBool("_isMoving", false);
            }

            if (!_nacelleScript._hasHitBottom || !_nacelleScript._isUpMax)
            {
                if (!_nacelleScript._hasHitBottom && _upAndDownAddition < 0)
                {
                        _nacelle.transform.Translate(0, ((_upAndDownAddition) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                }

                if (!_nacelleScript._isUpMax && _upAndDownAddition > 0)
                {
                        _nacelle.transform.Translate(0, ((_upAndDownAddition) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                }
            }
            else
            {
                _nacelle.transform.Translate(0, ((_upAndDownAddition) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
            }

            //movement 2nd attempt
            if (_hasHitLeft || _hasHitRight)
            {
                if (_hasHitLeft && _rightAndLeftAddition > 0)
                {
                    this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                }

                if (_hasHitRight && _rightAndLeftAddition < 0)
                {
                    this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                }
            }

            else
            {
                this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
            }

            if (!_nacelleScript._hasHitBottom)
            {
                _nacelle.transform.Translate(0, (_backgroundScrollScript.scrollSpeed - _backgroundScrollScript.acceleration) * Time.deltaTime, 0);

            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraGauche"))
        {
            _hasHitLeft = true;
        }

        if (collision.CompareTag("LimiteCameraDroite"))
        {
            _hasHitRight = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraGauche"))
        {
            _hasHitLeft = false;
        }

        if (collision.CompareTag("LimiteCameraDroite"))
        {
            _hasHitRight = false;
        }
    }
}
