﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{

    public float scrollSpeed;
    private Rigidbody2D body;
    public float acceleration = 0.5f;
    public float accelerationMomentum;
    private float _t = 0f;
    public float timeToAccelerate;
    //repeat background
    private float bgLength;

    private LifeController _lifeControllerScript;
    public float _timeToSlowOnDefeat;
    private float _timeToSlowOnDefeatTimer;

    public int totalWarnings;

    public GameObject gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _timeToSlowOnDefeatTimer = 0f;
        body = GetComponent<Rigidbody2D>();
        bgLength = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        _lifeControllerScript = GameObject.FindWithTag("GameController").GetComponent<LifeController>();
        //bgLength = gameObject.transform.localScale.y;
    }

    // Update is called once per frame
    void Update()
    {
        
        _t += Time.deltaTime;

        if (_t >= timeToAccelerate)
        {
            _t = 0f;
            if(_lifeControllerScript._life < totalWarnings && gameManager.GetComponent<ScoreController>().countDown < 4)
            {
                acceleration = acceleration + accelerationMomentum;
            }
        }
        if (_lifeControllerScript._life < totalWarnings /*&& (gameManager.GetComponent<ScoreController>().countDown == 1 || gameManager.GetComponent<ScoreController>().countDown == 2)*/)
        {
            body.velocity = new Vector2(0, scrollSpeed - acceleration);
        }
        else
        {
            if (body.velocity != new Vector2(0,0))
            {
                _timeToSlowOnDefeatTimer += Time.deltaTime;
                body.velocity = Vector2.Lerp(body.velocity, new Vector2(0, 0), _timeToSlowOnDefeatTimer / _timeToSlowOnDefeat); //a recheck pour que ça se finisse bien en _timeToSlowOnDefeat secondes, je crois que avec le fade de la velocity built in dans unity ça multiplie la vitesse de slow et donc finit pas le lerp.
                gameManager.GetComponent<LifeController>().isGameOver = true;
            }
        }


        if (transform.position.y < -bgLength)
        {
            bgReposition();
        }
    }

    private void bgReposition()
    {
        /*Vector2 fondOffset = new Vector2(0, bgLength * 3);
        transform.position = (Vector2)transform.position + fondOffset;*/
        Vector2 fondOffset = new Vector2(0, 13.8f*3);
        transform.position = (Vector2)transform.position + fondOffset;
    }
}
