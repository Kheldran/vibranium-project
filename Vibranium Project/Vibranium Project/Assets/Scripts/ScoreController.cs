﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public Text scoreUI;
    public Text highscoreUI;
    public int numberScore = 0;
    public int highscore;
    public int countDown = 1;
    public int firstPalier;
    public int secondPalier;
    public int thirdPalier;

    // Start is called before the first frame update
    void Start()
    {
        highscore = PlayerPrefs.GetInt("highscore", highscore);
        highscoreUI.text = "Best : " + highscore.ToString();
        countDown = 1;
    }

    // Update is called once per frame
    void Update()
    {
        scoreUI.text = numberScore.ToString("#00") + "m";
        if (numberScore == firstPalier)
        {
            countDown = 2;
        }
        else if(numberScore == secondPalier)
        {
            countDown = 3;
        }
        else if (numberScore >= thirdPalier)
        {
            countDown = 4;
        }

        //

        if (numberScore > highscore)
        {
            highscore = numberScore;
            highscoreUI.text = "Best : " + numberScore;

            PlayerPrefs.SetInt("highscore", highscore);
        }
    }
}
