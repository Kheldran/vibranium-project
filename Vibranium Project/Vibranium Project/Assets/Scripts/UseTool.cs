﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseTool : MonoBehaviour
{
    private Rigidbody2D body;
    private float speed = 4f;

    public string[] myTools;
    public string currentTool;
    public float cleanTimer = 0;
    private int toolIndex = 0;

    public RectTransform _toolSelectionUI;
    public GameObject[] _toolsUI;

    public float _toolsUISpeedToSlide;
    private float _toolsUiSpeed;

    private bool _hasChanged;
    private bool _isRightTriggerDown;
    private bool _isLeftTriggerDown;

    public AudioClip myClip;
    public AudioClip[] toolClips;
    private AudioSource myAudio;

    public GameObject _middleCleaner;
    private Animator _middleCleanerAnim;

    private int Xbox_One_Controller = 0;
    private int PS4_Controller = 0;

    private float _rightTrigger;
    private float _leftTrigger;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        currentTool = myTools[toolIndex];
        _toolSelectionUI.anchoredPosition = _toolsUI[toolIndex].GetComponent<RectTransform>().anchoredPosition;
        _isLeftTriggerDown = false;
        _isRightTriggerDown = false;
        myAudio = GetComponent<AudioSource>();
        _middleCleanerAnim = _middleCleaner.GetComponent<Animator>();

        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                PS4_Controller = 1;
                Xbox_One_Controller = 0;
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                //set a controller bool to true
                PS4_Controller = 0;
                Xbox_One_Controller = 1;

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Xbox_One_Controller == 1)
        {
            //permet de get l'input sur les boutons RT/LT de la manette
            _rightTrigger = Input.GetAxis("RT");
            _leftTrigger = Input.GetAxis("LT");
        }
        else if (PS4_Controller == 1)
        {
            //permet de get l'input sur les boutons RT/LT de la manette
            _rightTrigger = Input.GetAxis("R2");
            _leftTrigger = Input.GetAxis("L2");
        }


        // body.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed);
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (toolIndex != 0)
            {
                toolIndex -= 1;
                currentTool = myTools[toolIndex];
                _toolSelectionUI.transform.SetParent(_toolsUI[toolIndex].transform);
                _toolSelectionUI.anchoredPosition = Vector3.Lerp(_toolSelectionUI.anchoredPosition, new Vector3(0, 0, 0), _toolsUISpeedToSlide * Time.deltaTime);
                _hasChanged = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            if (toolIndex != myTools.Length - 1)
            {
                toolIndex += 1;
                currentTool = myTools[toolIndex];
                _toolSelectionUI.transform.SetParent(_toolsUI[toolIndex].GetComponent<RectTransform>());
                _hasChanged = true;
            }
        }

        //Le code ci-dessous est fonctionnel pour manette, je te laisse les contrôles clavier si t'en as besoin :)
        //sankyuu~~


        //si on appuie sur LT
        if (_leftTrigger > 0)
        {
            //check pour ne pas que ça passe plusieurs fois dans la boucle en 1 seul appuyage
            if (!_isLeftTriggerDown)
            {
                if (toolIndex != 0)
                {
                    toolIndex -= 1;
                    currentTool = myTools[toolIndex];
                    _middleCleanerAnim.SetBool("_isSearching", true);
                    //permet de modifier le parent de l'objet qui montre l'outil selectionné pour set sa position
                    _toolSelectionUI.transform.SetParent(_toolsUI[toolIndex].transform);
                    //permet de lancer le lerp pour le changement de position
                    _hasChanged = true;
                    //permet d'empecher plusieurs passage dans la boucle en 1 appuyage
                    _isLeftTriggerDown = true;
                }
            }
        }
        //same que au dessus pour RT
        else if (_rightTrigger > 0)
        {
            if (!_isRightTriggerDown)
            {

                if (toolIndex != myTools.Length - 1)
                {
                    toolIndex += 1;
                    currentTool = myTools[toolIndex];
                    _middleCleanerAnim.SetBool("_isSearching", true);

                    _toolSelectionUI.transform.SetParent(_toolsUI[toolIndex].GetComponent<RectTransform>());
                    _hasChanged = true;
                    _isRightTriggerDown = true;
                }
            }
        }

        //permet de pouvoir réappuyer sur les triggers après les avoir relachés
        if (_leftTrigger <= 0)
        {
            _isLeftTriggerDown = false;
        }

        if (_rightTrigger <= 0)
        {
            _isRightTriggerDown = false;
        }

        //permet de lerp la position de l'objet montrant l'outil selectionné sur la position de l'objet parent qui est modifié au dessus
        if (_hasChanged)
        {
            _toolsUiSpeed += _toolsUISpeedToSlide * Time.deltaTime;
            _toolSelectionUI.anchoredPosition = Vector3.Lerp(_toolSelectionUI.anchoredPosition, new Vector3(0, 0, 0), _toolsUiSpeed);
            if (_toolsUiSpeed > 1)
            {
                _toolsUiSpeed = 0;
                _middleCleanerAnim.SetBool("_isSearching", false);
                _hasChanged = false;
            }
        }
    }

    public void PlaySound(AudioClip thisAudio)
    {
        myAudio.loop = false;
        myAudio.enabled = false;
        myAudio.clip = thisAudio;
        myAudio.enabled = true;
    }
    public void PlaySoundTool(int toolNumber)
    {
        myAudio.enabled = false;
        myAudio.loop = true;
        myAudio.clip = toolClips[toolNumber];
        myAudio.enabled = true;
    }

    public void NoSound()
    {
        myAudio.enabled = false;
    }
}
