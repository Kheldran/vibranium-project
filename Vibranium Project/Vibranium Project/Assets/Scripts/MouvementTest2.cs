﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvementTest2 : MonoBehaviour
{

    private float _leftJoystickHorizontalValue;
    private float _leftJoystickVerticalValue;

    private float _rightJoystickHorizontalValue;
    private float _rightJoystickVerticalValue;

    private float _rightAndLeftAddition;

    public Camera _camera;

    private bool _hasHitLeft;
    private bool _hasHitRight;


    public GameObject _nacelle;
    public GameObject _player1;
    public GameObject _player2;



    private Nacelle _nacelleScript;

    public GameObject _backgroundScrollObject;
    private BackgroundScroll _backgroundScrollScript;

    public float _verticalSpeed;
    public float _speed;

    public GameObject _leftCleaner;
    public GameObject _rightCleaner;
    private Animator _rightCleanerAnim;
    private Animator _leftCleanerAnim;

    private GameButtons _gameButtonsScript;
    private LifeController _lifeControllerScript;

    private int Xbox_One_Controller = 0;
    private int PS4_Controller = 0;
    // Start is called before the first frame update
    void Start()
    {
        _nacelleScript = _nacelle.GetComponent<Nacelle>();
        _backgroundScrollScript = _backgroundScrollObject.GetComponent<BackgroundScroll>();
        _rightCleanerAnim = _rightCleaner.GetComponent<Animator>();
        _leftCleanerAnim = _leftCleaner.GetComponent<Animator>();
        _gameButtonsScript = GameObject.FindWithTag("GameController").GetComponent<GameButtons>();
        _lifeControllerScript = GameObject.FindWithTag("GameController").GetComponent<LifeController>();
        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                PS4_Controller = 1;
                Xbox_One_Controller = 0;
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                //set a controller bool to true
                PS4_Controller = 0;
                Xbox_One_Controller = 1;

            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (!_gameButtonsScript._isPauseOn && !_lifeControllerScript.isGameOver)
        {
            if (Xbox_One_Controller == 1)
            {

                Vector3 _screenPosition = _camera.WorldToScreenPoint(this.transform.position);

                _rightJoystickHorizontalValue = Input.GetAxis("HorizontalRightJoystick");
                _rightJoystickVerticalValue = Input.GetAxis("VerticalRightJoystick");

                _leftJoystickHorizontalValue = Input.GetAxis("Horizontal");
                _leftJoystickVerticalValue = Input.GetAxis("VerticalLeft");

                _rightAndLeftAddition = _rightJoystickHorizontalValue + _leftJoystickHorizontalValue;

                if (_rightJoystickHorizontalValue != 0 || _rightJoystickVerticalValue != 0)
                {
                    _rightCleanerAnim.SetBool("_isMoving", true);
                }
                else if (_rightJoystickHorizontalValue == 0 && _rightJoystickVerticalValue == 0)
                {
                    _rightCleanerAnim.SetBool("_isMoving", false);
                }

                if (_leftJoystickHorizontalValue != 0 || _leftJoystickVerticalValue != 0)
                {
                    _leftCleanerAnim.SetBool("_isMoving", true);
                }
                else if (_leftJoystickHorizontalValue == 0 && _leftJoystickVerticalValue == 0)
                {
                    _leftCleanerAnim.SetBool("_isMoving", false);
                }

                //movement 2nd attempt
                if (!_nacelleScript._hasHitBottom || !_nacelleScript._isUpMax)
                {
                    if (!_nacelleScript._hasHitBottom)
                    {
                        if (_rightJoystickVerticalValue + _leftJoystickVerticalValue > 0)
                        {
                            _nacelle.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                        }
                    }

                    if (!_nacelleScript._isUpMax)
                    {
                        if (_rightJoystickVerticalValue + _leftJoystickVerticalValue < 0)
                        {
                            _nacelle.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                        }
                    }
                }
                else
                {
                    _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                }

                if (!_nacelleScript._hasHitBottom)
                {
                    _nacelle.transform.Translate(0, (_backgroundScrollScript.scrollSpeed - _backgroundScrollScript.acceleration) * Time.deltaTime, 0);

                }

                //movement 2nd attempt
                if (_hasHitLeft || _hasHitRight)
                {
                    if (_hasHitLeft)
                    {
                        if (_rightAndLeftAddition < 0)
                        {
                            this.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                        }
                    }

                    if (_hasHitRight)
                    {
                        if (_rightAndLeftAddition > 0)
                        {
                            this.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                        }
                    }
                }

                else
                {
                    this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                }
            }
        }
        if (!_gameButtonsScript._isPauseOn && !_lifeControllerScript.isGameOver)
        { 
            if (PS4_Controller == 1)
            {

                Vector3 _screenPosition = _camera.WorldToScreenPoint(this.transform.position);

                _rightJoystickHorizontalValue = Input.GetAxis("HorizontalRightJoystickPS4");
                _rightJoystickVerticalValue = Input.GetAxis("VerticalRightJoystickPS4");

                _leftJoystickHorizontalValue = Input.GetAxis("Horizontal");
                _leftJoystickVerticalValue = Input.GetAxis("VerticalLeft");

                _rightAndLeftAddition = _rightJoystickHorizontalValue + _leftJoystickHorizontalValue;

                if (_rightJoystickHorizontalValue != 0 || _rightJoystickVerticalValue != 0)
                {
                    _rightCleanerAnim.SetBool("_isMoving", true);
                }
                else if (_rightJoystickHorizontalValue == 0 && _rightJoystickVerticalValue == 0)
                {
                    _rightCleanerAnim.SetBool("_isMoving", false);
                }

                if (_leftJoystickHorizontalValue != 0 || _leftJoystickVerticalValue != 0)
                {
                    _leftCleanerAnim.SetBool("_isMoving", true);
                }
                else if (_leftJoystickHorizontalValue == 0 && _leftJoystickVerticalValue == 0)
                {
                    _leftCleanerAnim.SetBool("_isMoving", false);
                }

                //movement 2nd attempt
                if (!_nacelleScript._hasHitBottom || !_nacelleScript._isUpMax)
                {
                    if (!_nacelleScript._hasHitBottom)
                    {
                        if (_rightJoystickVerticalValue + _leftJoystickVerticalValue > 0)
                        {
                            _nacelle.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                        }
                    }

                    if (!_nacelleScript._isUpMax)
                    {
                        if (_rightJoystickVerticalValue + _leftJoystickVerticalValue < 0)
                        {
                            _nacelle.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                        }
                    }
                }
                else
                {
                    _nacelle.transform.Translate(0, ((_rightJoystickVerticalValue + _leftJoystickVerticalValue) * _verticalSpeed) * _backgroundScrollScript.acceleration, 0);
                }

                if (!_nacelleScript._hasHitBottom)
                {
                    _nacelle.transform.Translate(0, (_backgroundScrollScript.scrollSpeed - _backgroundScrollScript.acceleration) * Time.deltaTime, 0);

                }

                //movement 2nd attempt
                if (_hasHitLeft || _hasHitRight)
                {
                    if (_hasHitLeft)
                    {
                        if (_rightAndLeftAddition < 0)
                        {
                            this.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                        }
                    }

                    if (_hasHitRight)
                    {
                        if (_rightAndLeftAddition > 0)
                        {
                            this.transform.Translate(0, 0, 0);
                        }
                        else
                        {
                            this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                        }
                    }
                }

                else
                {
                    this.transform.Translate((_rightAndLeftAddition * _speed) * _backgroundScrollScript.acceleration, 0, 0);
                }
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraGauche"))
        {
            _hasHitLeft = true;
        }

        if (collision.CompareTag("LimiteCameraDroite"))
        {
            _hasHitRight = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraGauche"))
        {
            _hasHitLeft = false;
        }

        if (collision.CompareTag("LimiteCameraDroite"))
        {
            _hasHitRight = false;
        }
    }
}
