﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer1 : MonoBehaviour
{

    public float _speed;
    public float _bonusSpeed;

    public float _verticalSpeed;

    private float _leftJoystickHorizontalValue;
    private float _leftJoystickVerticalValue;

    public GameObject _nacelle;
    private Nacelle _nacelleScript;

    public GameObject _backgroundScrollObject;
    private BackgroundScroll _backgroundScrollScript;

    private bool _hasHitBottom;

    public GameObject _leftCleanerMan;
    private Animator _leftCleanerAnimator;

    public float _verticalJoystickValueMinToRegisterInput;
    private int Xbox_One_Controller = 0;
    private int PS4_Controller = 0;

    private float _distanceHitLeft;
    
    // Start is called before the first frame update
    void Start()
    {
        _nacelleScript = _nacelle.GetComponent<Nacelle>();
        _backgroundScrollScript = _backgroundScrollObject.GetComponent<BackgroundScroll>();
        _leftCleanerAnimator = _leftCleanerMan.GetComponent<Animator>();
        
        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                PS4_Controller = 1;
                Xbox_One_Controller = 0;
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                //set a controller bool to true
                PS4_Controller = 0;
                Xbox_One_Controller = 1;

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Xbox_One_Controller == 1)
        {

            
            _leftJoystickHorizontalValue = Input.GetAxis("Horizontal");
            _leftJoystickVerticalValue = Input.GetAxis("VerticalLeft");


            if (_leftJoystickHorizontalValue != 0 || _leftJoystickVerticalValue != 0)
            {
                _leftCleanerAnimator.SetBool("_isMoving", true);
                _leftCleanerAnimator.speed = _backgroundScrollScript.acceleration;
            }
            else if (_leftJoystickHorizontalValue == 0 && _leftJoystickVerticalValue == 0)
            {
                _leftCleanerAnimator.SetBool("_isMoving", false);
                _leftCleanerAnimator.speed = 1;
            }

        


            if (!_nacelleScript._isLeftMax )
            {
                if (_leftJoystickHorizontalValue < -0.2f)
                    //controle pour les déplacements droite-gauche
                     //if (Input.GetKey(KeyCode.Q))
                {
                    Debug.Log("Hit");
                    //enleve le freeze position x sur le rigidbody pour permettre l'application de la force tant qu'on press le button
                    this.GetComponent<Rigidbody2D>().constraints = ~RigidbodyConstraints2D.FreezePositionX;
                    //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_speed * (_backgroundScrollScript.acceleration * 0.5f), 0), ForceMode2D.Force);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-_speed * (_backgroundScrollScript.acceleration * 0.5f), 0);
                    _nacelle.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_bonusSpeed * (_backgroundScrollScript.acceleration * 0.5f),0));

                    //print(GetComponent<SpringJoint2D>().reactionForce);
                }
            }
            if (_leftJoystickHorizontalValue == 0)
                //quand on release le button, refreeze la position x et réinitialise la force appliqué au rigidbody
            //if (Input.GetKeyUp(KeyCode.Q))
            {
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
            if (!_nacelleScript._isRightMax)
            {
                if (_leftJoystickHorizontalValue > 0.2f)
                //same que au dessus
                //if (Input.GetKey(KeyCode.D))
                {
                    print("working");

                    this.GetComponent<Rigidbody2D>().constraints = ~RigidbodyConstraints2D.FreezePositionX;
                    //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(_speed * (_backgroundScrollScript.acceleration * 0.5f), 0), ForceMode2D.Force);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(_speed * (_backgroundScrollScript.acceleration * 0.5f), 0);
                    _nacelle.GetComponent<Rigidbody2D>().AddForce(new Vector2(_bonusSpeed * (_backgroundScrollScript.acceleration * 0.5f), 0));
                }
            }

            if (_leftJoystickHorizontalValue == 0)
            //if (Input.GetKeyUp(KeyCode.D))
            {
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }

            if (!_nacelleScript._isUpMax)
            {
                if (_leftJoystickVerticalValue > _verticalJoystickValueMinToRegisterInput)
                //réduit ou augmente la distance du joint qui sépare la nacelle des controleurs en fonction du bouton appuyé
                //if (Input.GetKey(KeyCode.Z))
                {
                    GetComponent<SpringJoint2D>().distance -= _verticalSpeed * Time.deltaTime * (_backgroundScrollScript.acceleration * 0.5f);


                    //permet de refresh la distance entre les deux objets à chaque mouvement
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0), ForceMode2D.Impulse);
                }

            }
            if (!_nacelleScript._isDownMax || _nacelle.transform.rotation.z < -0.01)
            {
                if (_leftJoystickVerticalValue < -_verticalJoystickValueMinToRegisterInput)
                // if (Input.GetKey(KeyCode.S))
                {
                    GetComponent<SpringJoint2D>().distance += _verticalSpeed * Time.deltaTime * (_backgroundScrollScript.acceleration * 0.5f);

                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0), ForceMode2D.Impulse);
                }


            }
            else
            {
                GetComponent<SpringJoint2D>().distance = Vector2.Distance(this.transform.position, _nacelle.transform.GetChild(1).transform.position);
            }
        }

        else if (PS4_Controller == 1)
        {


            _leftJoystickHorizontalValue = Input.GetAxis("Horizontal");
            _leftJoystickVerticalValue = Input.GetAxis("VerticalLeft");


            if (_leftJoystickHorizontalValue != 0 || _leftJoystickVerticalValue != 0)
            {
                _leftCleanerAnimator.SetBool("_isMoving", true);
                _leftCleanerAnimator.speed = _backgroundScrollScript.acceleration;
            }
            else if (_leftJoystickHorizontalValue == 0 && _leftJoystickVerticalValue == 0)
            {
                _leftCleanerAnimator.SetBool("_isMoving", false);
                _leftCleanerAnimator.speed = 1;
            }




            if (!_nacelleScript._isLeftMax)
            {

                if (_leftJoystickHorizontalValue < -0.2f)
                //controle pour les déplacements droite-gauche
                //if (Input.GetKey(KeyCode.Q))
                {
                    Debug.Log("Hit");
                    //enleve le freeze position x sur le rigidbody pour permettre l'application de la force tant qu'on press le button
                    this.GetComponent<Rigidbody2D>().constraints = ~RigidbodyConstraints2D.FreezePositionX;
                    //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_speed * (_backgroundScrollScript.acceleration * 0.5f), 0), ForceMode2D.Force);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-_speed * (_backgroundScrollScript.acceleration * 0.5f), 0);
                    _nacelle.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_bonusSpeed * (_backgroundScrollScript.acceleration * 0.5f), 0));

                    //print(GetComponent<SpringJoint2D>().reactionForce);
                }
            }
            if (_leftJoystickHorizontalValue == 0)
            //quand on release le button, refreeze la position x et réinitialise la force appliqué au rigidbody
            //if (Input.GetKeyUp(KeyCode.Q))
            {
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
            if (!_nacelleScript._isRightMax)
            {
                
                if (_leftJoystickHorizontalValue > 0.2f)
                //same que au dessus
                //if (Input.GetKey(KeyCode.D))
                {
                    this.GetComponent<Rigidbody2D>().constraints = ~RigidbodyConstraints2D.FreezePositionX;
                    //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(_speed * (_backgroundScrollScript.acceleration * 0.5f), 0), ForceMode2D.Force);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(_speed * (_backgroundScrollScript.acceleration * 0.5f), 0);
                    _nacelle.GetComponent<Rigidbody2D>().AddForce(new Vector2(_bonusSpeed * (_backgroundScrollScript.acceleration * 0.5f), 0));

                }
            }
            if (_leftJoystickHorizontalValue == 0)
            //if (Input.GetKeyUp(KeyCode.D))
            {
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }

            if (!_nacelleScript._isUpMax)
            {
                if (_leftJoystickVerticalValue > _verticalJoystickValueMinToRegisterInput)
                //réduit ou augmente la distance du joint qui sépare la nacelle des controleurs en fonction du bouton appuyé
                //if (Input.GetKey(KeyCode.Z))
                {
                    GetComponent<SpringJoint2D>().distance -= _verticalSpeed * Time.deltaTime * (_backgroundScrollScript.acceleration * 0.5f);


                    //permet de refresh la distance entre les deux objets à chaque mouvement
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0), ForceMode2D.Impulse);
                }

            }


            if ((!_nacelleScript._isDownMax || _nacelle.transform.rotation.z < -0.01) && !_nacelleScript._isRightMax)
            {
                Debug.Log("works");

                if (_leftJoystickVerticalValue < -_verticalJoystickValueMinToRegisterInput)
                // if (Input.GetKey(KeyCode.S))
                {
                    GetComponent<SpringJoint2D>().distance += _verticalSpeed * Time.deltaTime * (_backgroundScrollScript.acceleration * 0.5f);
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0), ForceMode2D.Impulse);
                }


            }
            else
            {
                GetComponent<SpringJoint2D>().distance = Vector2.Distance(this.transform.position, _nacelle.transform.GetChild(1).transform.position);
            }
        }

        if (!_nacelleScript._hasHitBottom)
        {
            GetComponent<SpringJoint2D>().distance -= (_backgroundScrollScript.scrollSpeed - _backgroundScrollScript.acceleration)  * Time.deltaTime;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0), ForceMode2D.Impulse);

        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraDroite"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }

        if (collision.CompareTag("LimiteCameraGauche"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        }


    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraDroite"))
        {
            if (this.GetComponent<Rigidbody2D>().velocity.x > 0)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }
        }

        if (collision.CompareTag("LimiteCameraGauche"))
        {
            if (this.GetComponent<Rigidbody2D>().velocity.x < 0)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            }
        }
    }


}
