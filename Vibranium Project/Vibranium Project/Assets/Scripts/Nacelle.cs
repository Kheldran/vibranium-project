﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nacelle : MonoBehaviour
{

    public Camera MainCamera;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;

    public bool _isDownMax { get; private set; }
    public bool _isUpMax { get; private set; }
    public bool _isLeftMax { get; private set; }
    public bool _isRightMax { get; private set; }

    public float _limiteNacelleBas;

    public bool _hasHitBottom { get;  set; }



    // Start is called before the first frame update
    void Start()
    {
        //screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        //objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; //extents = size of width / 2
        //objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2
        _isDownMax = false;
        _isUpMax = false;
        _isLeftMax = false;
        _isRightMax = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 _screenPosition = MainCamera.WorldToScreenPoint(this.transform.position);

        //if (_screenPosition.y <= _limiteNacelleBas)
        //{
        //    _isDownMax = true;
        //}
        //else
        //{
        //    _isDownMax = false;
        //}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraDroite"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            _isRightMax = true;
        }

        if (collision.CompareTag("LimiteCameraGauche"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            _isLeftMax = true;
        }

        if (collision.CompareTag("LimiteCameraBas"))
        {
            _hasHitBottom = true;
            _isDownMax = true;
        }

        if (collision.CompareTag("LimiteCameraHaut"))
        {
            _isUpMax = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraDroite"))
        {
            if (this.GetComponent<Rigidbody2D>().velocity.x > 0)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }
        }

        if (collision.CompareTag("LimiteCameraGauche"))
        {
            if (this.GetComponent<Rigidbody2D>().velocity.x < 0)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("LimiteCameraDroite"))
        {
            _isRightMax = false;
        }

        if (collision.CompareTag("LimiteCameraGauche"))
        {
            _isLeftMax = false;
        }
        if (collision.CompareTag("LimiteCameraBas"))
        {
            _hasHitBottom = false;
            _isDownMax = false;
        }
        if (collision.CompareTag("LimiteCameraHaut"))
        {
            _isUpMax = false;
        }
    }
}
