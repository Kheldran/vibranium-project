﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilBehaviour : MonoBehaviour
{

    public GameObject _origineFil;
    public GameObject _joueur;

    private float _distance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        this.transform.LookAt(_joueur.transform);
       
        Stretch(_joueur.transform.position, _origineFil.transform.position);

    }

    void Stretch(Vector3 startPos, Vector3 endPos)
    {
        _distance = _joueur.transform.parent.GetComponent<SpringJoint2D>().distance;

        Vector3 centerPos = new Vector3(startPos.x + endPos.x, startPos.y + endPos.y) / 2;

        this.transform.position = centerPos;
        this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, _distance * 2);

    }
}
