﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuButtons : MonoBehaviour
{
    public GameObject creditsPanel;
    public GameObject buttonsPanel;
    public GameObject scenePanel;
    public GameObject tutoPanel;

    public Button btnBack;
    public Button btnPlay;
    public Button btnMat;
    public Button btnBackTuto;

    private AudioSource myAudio;
    public AudioClip myClip;

    private int Xbox_One_Controller = 0;
    private int PS4_Controller = 0;

    public string sceneName;
    public GameObject _eventSystem;

    public GameObject logo;

    // Start is called before the first frame update
    void Start()
    {
        creditsPanel.SetActive(false);
        myAudio = GetComponent<AudioSource>();

        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                PS4_Controller = 1;
                Xbox_One_Controller = 0;

                _eventSystem.GetComponent<MyInputModule>().submitButton = "SubmitPS4";
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                //set a controller bool to true
                PS4_Controller = 0;
                Xbox_One_Controller = 1;
                _eventSystem.GetComponent<MyInputModule>().submitButton = "SubmitXbox";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void Play()
    {
        myAudio.PlayOneShot(myClip);
        logo.SetActive(false);
        buttonsPanel.SetActive(false);
        scenePanel.SetActive(true);
        btnMat.Select();
    }
    //public void ChooseSceneMatt()
    //{
    //    myAudio.PlayOneShot(myClip);
    //    SceneManager.LoadScene("Matt_2");
    //}
    public void ChooseSceneTest()
    {
        myAudio.PlayOneShot(myClip);
        SceneManager.LoadScene("TestScene");
    }
    public void ChooseSceneProut()
    {
        myAudio.PlayOneShot(myClip);
        SceneManager.LoadScene(sceneName);
    }
    public void ChooseSceneJunior()
    {
        myAudio.PlayOneShot(myClip);
        SceneManager.LoadScene("TestScene3");
    }
    public void Credits()
    {
        myAudio.PlayOneShot(myClip);
        creditsPanel.SetActive(true);
        buttonsPanel.SetActive(false);
        btnBack.Select();
    }
    public void BackCredits()
    {
        myAudio.PlayOneShot(myClip);
        logo.SetActive(true);
        creditsPanel.SetActive(false);
        scenePanel.SetActive(false);
        buttonsPanel.SetActive(true);
        btnPlay.Select();
    }
    public void Tuto()
    {
        myAudio.PlayOneShot(myClip);
        tutoPanel.SetActive(true);
        scenePanel.SetActive(false);
        btnBackTuto.Select();
    }
    public void BackTuto()
    {
        myAudio.PlayOneShot(myClip);
        tutoPanel.SetActive(false);
        scenePanel.SetActive(true);
        //buttonsPanel.SetActive(true);
        btnMat.Select();
    }
    public void QuitGame()
    {
        myAudio.PlayOneShot(myClip);
        Application.Quit();
    }
}
