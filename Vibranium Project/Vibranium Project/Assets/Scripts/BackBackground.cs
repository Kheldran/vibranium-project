﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackBackground : MonoBehaviour
{
    public GameObject _background;
    private BackgroundScroll _backgroundScript;

    private Rigidbody2D body;
    // Start is called before the first frame update
    void Start()
    {
        _backgroundScript = _background.GetComponent<BackgroundScroll>();
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        body.velocity = new Vector2(0, _backgroundScript.scrollSpeed - _backgroundScript.acceleration);
    }
}
