﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameButtons : MonoBehaviour
{
    public GameObject pauseScreen;
    public Button resume;
    public EventSystem evtSystemRef;
    public bool _isPauseOn { get; private set; }

    private AudioSource myAudio;
    public AudioClip myClip;

    public GameObject music;
    private int Xbox_One_Controller = 0;
    private int PS4_Controller = 0;

    public GameObject _eventSystem;
    // Start is called before the first frame update
    void Start()
    {
        myAudio = GetComponent<AudioSource>();

        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                PS4_Controller = 1;
                Xbox_One_Controller = 0;

                _eventSystem.GetComponent<StandaloneInputModule>().submitButton = "SubmitPS4";

            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                //set a controller bool to true
                PS4_Controller = 0;
                Xbox_One_Controller = 1;
                _eventSystem.GetComponent<StandaloneInputModule>().submitButton = "SubmitXbox";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Xbox_One_Controller == 1)
        {
            if (Input.GetKeyDown("joystick button 7") || Input.GetKeyDown(KeyCode.M))
            {
                pauseScreen.SetActive(true);
                music.GetComponent<AudioSource>().Pause();
                resume.Select();
                Time.timeScale = 0;
                _isPauseOn = true;
            }
        }
        else if (PS4_Controller == 1)
        {
            if (Input.GetKeyDown("joystick button 9") || Input.GetKeyDown(KeyCode.M))
            {
                pauseScreen.SetActive(true);
                music.GetComponent<AudioSource>().Pause();
                resume.Select();
                Time.timeScale = 0;
                _isPauseOn = true;
            }
        }
    }

    public void Resume()
    {
        music.GetComponent<AudioSource>().UnPause();
        myAudio.PlayOneShot(myClip);
        pauseScreen.SetActive(false);
        evtSystemRef.SetSelectedGameObject(null);
        Time.timeScale = 1;
        _isPauseOn = false;
    }
    public void Restart()
    {
        myAudio.PlayOneShot(myClip);
        pauseScreen.SetActive(false);
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        _isPauseOn = false;
    }
    public void MainMenu()
    {
        myAudio.PlayOneShot(myClip);
        pauseScreen.SetActive(false);
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
        _isPauseOn = false;
    }
}
