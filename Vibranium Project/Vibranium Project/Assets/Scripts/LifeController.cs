﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeController : MonoBehaviour
{

    public int _life;
    //
    [HideInInspector] public bool isGameOver = false;
    public GameObject gameOverPanel;
    public Button restart;
    private int transition = 0;

    private AudioSource myAudio;

    public Sprite warningBad;
    public Image[] avertissements;
    private int previousLife = 0;

    public GameObject music;
    public GameObject cleanerMan;

    private void Start()
    {
        transition = 0;
        myAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_life > previousLife)
        {
            avertissements[previousLife].GetComponent<Image>().sprite = warningBad;
            previousLife += 1;
        }
        if (isGameOver)
        {
            cleanerMan.GetComponent<AudioSource>().enabled = false;
            gameOverPanel.SetActive(true);
            music.SetActive(false);
            transition += 1;
        }
        if (transition == 1)
        {
            Time.timeScale = 0;
            restart.Select();
            isGameOver = false;
        }
    }
    public void PlayWarning()
    {
        myAudio.enabled = false;
        myAudio.enabled = true;
    }
}
