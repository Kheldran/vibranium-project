﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hazards : MonoBehaviour
{
    public float cleanTimeDefault = 1f;
    public AudioClip myAudioClip;
    public AudioClip myGoodAudioClip;

    private LifeController _lifeControllerScript;

    public RectTransform cleanBarForeground;
    public GameObject cleanBar;
    private float cleanBarx;

    [HideInInspector]public int needToAccelerate;
    private GameObject gameController;

    public Sprite[] toolsSprites;

    private int currentToolUsed;

    // Start is called before the first frame update
    void Start()
    {
        _lifeControllerScript = GameObject.FindWithTag("GameController").GetComponent<LifeController>();
        gameController = GameObject.FindWithTag("GameController");
        cleanBarx = cleanBarForeground.sizeDelta.x;
    }

    // Update is called once per frame
    void Update()
    {
        cleanBarForeground.sizeDelta = new Vector2(cleanTimeDefault * cleanBarx, cleanBarForeground.sizeDelta.y);
        needToAccelerate = gameController.GetComponent<ScoreController>().countDown;
        //needToAccelerate = 1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            cleanBar.SetActive(true);
            collision.gameObject.GetComponent<UseTool>().PlaySoundTool(int.Parse(collision.gameObject.GetComponent<UseTool>().currentTool));
            currentToolUsed = int.Parse(collision.gameObject.GetComponent<UseTool>().currentTool);

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            if (currentToolUsed != int.Parse(collision.gameObject.GetComponent<UseTool>().currentTool))
            {
                collision.gameObject.GetComponent<UseTool>().PlaySoundTool(int.Parse(collision.gameObject.GetComponent<UseTool>().currentTool));
                currentToolUsed = int.Parse(collision.gameObject.GetComponent<UseTool>().currentTool);
            }

            collision.GetComponent<Animator>().SetBool("_isCleaning", true);
            if (collision.gameObject.GetComponent<UseTool>().currentTool == gameObject.tag)
            {
                collision.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().gameObject.SetActive(false);
                cleanTimeDefault -= (Time.deltaTime) * needToAccelerate;
                if (cleanTimeDefault <= 0)
                {
                    collision.gameObject.GetComponent<UseTool>().PlaySound(myAudioClip);
                    collision.gameObject.transform.parent.GetChild(0).GetComponent<AudioSource>().Play();
                    Destroy(gameObject);
                    collision.GetComponent<Animator>().SetBool("_isCleaning", false);
                }
             }
             else
             {
                collision.gameObject.transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = toolsSprites[int.Parse(gameObject.tag)];
                collision.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().gameObject.SetActive(true);
                cleanTimeDefault -= (Time.deltaTime/2) * needToAccelerate;
                if (cleanTimeDefault <= 0)
                {
                    collision.gameObject.GetComponent<UseTool>().PlaySound(myAudioClip);
                    collision.gameObject.transform.parent.GetChild(0).GetComponent<AudioSource>().Play();

                    Destroy(gameObject);
                    collision.GetComponent<Animator>().SetBool("_isCleaning", false);
                }
             }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlaceToDestroy")
        {
            if (_lifeControllerScript._life < 2)
            {
                _lifeControllerScript.PlayWarning();
            }
            _lifeControllerScript._life += 1;
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.transform.GetChild(2).transform.GetChild(0).GetComponent<Image>().gameObject.SetActive(false);
            collision.GetComponent<Animator>().SetBool("_isCleaning", false);
            cleanBar.SetActive(false);
            collision.gameObject.GetComponent<UseTool>().NoSound();
        }
    }
}
